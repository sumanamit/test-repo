const { addition, subtraction, multiplication, division } = require('.');

describe('Basic Arithmatic operations', () => {
  it('addition - with param', async () => {
    const result = await addition(18, 6);
    expect(result).toBe(18 + 6);
  });

  it('addition - without param', async () => {
    await addition().catch((e) => {
      expect(e).toBe('Insufficient arguments');
    });
  });

  it('subtraction - with param', async () => {
    const result = await subtraction(18, 6);
    expect(result).toBe(18 - 6);
  });

  it('subtraction - without param', async () => {
    await subtraction().catch((e) => {
      expect(e).toBe('Insufficient arguments');
    });
  });

  it('multiplication - with param', async () => {
    const result = await multiplication(18, 6);
    expect(result).toBe(18 * 6);
  });

  it('multiplication - without param', async () => {
    await multiplication().catch((e) => {
      expect(e).toBe('Insufficient arguments');
    });
  });

  it('division - with param', async () => {
    const result = await division(18, 3);
    expect(result).toBe(18 / 3);
  });

  it('division - with no parameter', async () => {
    await division().catch((e) => {
      expect(e).toBe('Insufficient arguments');
    });
  });
});
