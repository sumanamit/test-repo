const INSUFFICIENT_ARGUMENTS = 'Insufficient arguments';

const addition = async (firstNumber, secondNumber) => {
  if (firstNumber && secondNumber) {
    return firstNumber + secondNumber;
  }
  throw INSUFFICIENT_ARGUMENTS;
};

const subtraction = async (firstNumber, secondNumber) => {
  if (firstNumber && secondNumber) {
    return firstNumber - secondNumber;
  }
  throw INSUFFICIENT_ARGUMENTS;
};

const division = async (firstNumber, secondNumber) => {
  if (firstNumber && secondNumber) {
    return firstNumber / secondNumber;
  }
  throw INSUFFICIENT_ARGUMENTS;
};

const multiplication = async (firstNumber, secondNumber) => {
  if (firstNumber && secondNumber) {
    return firstNumber * secondNumber;
  }
  throw INSUFFICIENT_ARGUMENTS;
};

module.exports = {
  addition,
  subtraction,
  multiplication,
  division,
};
